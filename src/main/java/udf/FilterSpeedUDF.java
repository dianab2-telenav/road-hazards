package udf;

import data_processing.GeohashTransformation;
import data_processing.RowSeqToMap;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.api.java.UDF2;
import scala.Tuple3;
import scala.collection.JavaConversions;
import scala.collection.Seq;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class FilterSpeedUDF implements UDF2<Seq<Row>, Double, Seq<String>> {

    @Override public Seq<String> call(final Seq<Row> rowSeq, final Double speedMin) throws Exception {

        final List<Tuple3<Double, Double, Double>> converted = RowSeqToMap.convert(rowSeq);
        List<String> geoCodes = new ArrayList<>();
        GeohashTransformation geohashTransformation = new GeohashTransformation();
        for(int index = 1; index < converted.size() - 1; index ++) {
            Double dynamic = (converted.get(index)._1() - converted.get(index - 1)._1())/converted.get(index - 1)._1();
            if( dynamic < - 0.3 || converted.get(index)._1() <= speedMin)
                geoCodes.add(geohashTransformation.getGeohash(converted.get(index)._2(), converted.get(index)._3(), 8));
        }
        if(geoCodes.size()>0)
        System.out.println(converted.size() + " " + geoCodes.size() + " " + geoCodes.stream().distinct().collect(
                Collectors.toList()).size());
        return JavaConversions.asScalaBuffer(geoCodes).toSeq();
    }
}
