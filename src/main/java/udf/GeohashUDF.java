package udf;

import data_processing.GeohashTransformation;
import org.apache.spark.sql.api.java.UDF3;


public class GeohashUDF implements UDF3<Double, Double, Integer, String> {

    @Override public String call(final Double lat, final Double lon, Integer precision) throws Exception {
        String geoHash = null;
        GeohashTransformation geohashTransformation = new GeohashTransformation();
        geoHash = geohashTransformation.getGeohash(lat, lon, precision);
        return geoHash;
    }
}
