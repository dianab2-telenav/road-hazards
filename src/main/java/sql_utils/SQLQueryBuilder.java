package sql_utils;

import scala.Tuple2;

import java.util.Optional;


public class SQLQueryBuilder {
    public String buildSelectAll(String tableName, Optional<Tuple2<Object, Object>> criteria) {
        String query = "SELECT * FROM " + tableName;
        if(criteria.isPresent()) {
            query += "WHERE " + criteria.get()._1() + " = " + criteria.get()._2() + ";";
        }
        return query;
    }

}
