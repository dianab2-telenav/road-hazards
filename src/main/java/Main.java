import data_access.JsonReader;
import data_processing.DataFilter;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import udf.FilterSpeedUDF;
import udf.GeohashUDF;

import static org.apache.spark.sql.functions.*;


public class Main {

    private static final int GEO_ORDER = 7;
    public static void main(String args[]) {
        final SparkSession sparkSession = SparkSession.builder()
                .master("local[*]")
                .config("spark.sql.parquet.binaryAsString", "true")
                .appName("road_hazards")
                .getOrCreate();
        sparkSession.sparkContext().setLogLevel("ERROR");
        Main main = new Main();
        final Dataset<Row> probes = main.read(sparkSession);
        main.selectSpeedDecrease(probes, sparkSession);

    }

    public Dataset<Row> read(SparkSession sparkSession) {
        JsonReader jsonReader = new JsonReader();
        return jsonReader.read(sparkSession, "./src/main/resources/denali-06-internship/test-data");
    }
    public void selectStopped(Dataset<Row> probes, SparkSession sparkSession) {
        DataFilter dataFilter = new DataFilter();
        probes.sqlContext().registerDataFrameAsTable(probes, "probes");

        Dataset<Row> stoppedEngine = dataFilter.filter(sparkSession, probes, "SELECT * FROM probes WHERE payload.event_name = 'STOP_ENGINE'");
        sparkSession.sqlContext().udf().register("GeoUDF", new GeohashUDF(), DataTypes.StringType);
        final Dataset<Row> rowDataset = stoppedEngine.withColumn("geohash",
                callUDF("GeoUDF", col("payload.log_context.current_lat"), col("payload.log_context.current_lon"),
                        lit(GEO_ORDER)));
        //rowDataset.groupBy(col("geohash")).count().show();

        probes.printSchema();
    }

    public void selectSpeedDecrease(Dataset<Row> probes, SparkSession sparkSession) {
        probes.sqlContext().registerDataFrameAsTable(probes, "probes");
        DataFilter dataFilter = new DataFilter();
        Dataset<Row> GPSProbes = dataFilter.filter(sparkSession, probes, "SELECT * FROM probes WHERE payload.event_name = 'GPS_PROBES'");

        sparkSession.sqlContext().udf().register("SpeedUDF", new FilterSpeedUDF(), DataTypes.createArrayType(DataTypes.StringType));
        GPSProbes.withColumn("speed_zero", callUDF("SpeedUDF", col("payload.probe_list"), lit(2.0)))
                .filter(row -> row.getList(row.fieldIndex("speed_zero")).size() != 0)
                .select(col("speed_zero")).withColumn("geohash", explode(col("speed_zero"))).groupBy("geohash").count().sort(col("count")).show(1000000, false);
    }
}
