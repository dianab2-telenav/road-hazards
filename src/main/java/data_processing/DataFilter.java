package data_processing;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;


public class DataFilter {

    public Dataset<Row> filter(SparkSession sparkSession, Dataset<Row> probes, String query) {
        final Dataset<Row> probes1 = probes.sqlContext().sql(query);
        System.out.println(query + " " + probes1.count());
        return probes1;
    }


}
