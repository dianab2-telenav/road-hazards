package data_processing;

import static ch.hsr.geohash.GeoHash.withCharacterPrecision;

public class GeohashTransformation {

    public String getGeohash(final double lat, final double lon, final int precision) {

        String geoHash = "UNKNOWN";
        try {
            geoHash = withCharacterPrecision(lat, lon, precision).toBase32();
        } catch (Exception e) {
            //
        }
        return geoHash;

    }

}
