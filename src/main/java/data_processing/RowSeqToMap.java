package data_processing;


import org.apache.spark.sql.Row;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.collection.JavaConversions;
import scala.collection.Seq;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class RowSeqToMap implements Serializable {

    public static List<Tuple3<Double, Double, Double>> convert(final Seq<Row> rowSeq) {
        final Iterator<Row> iterator = rowSeq.iterator();
        List<Row> rowsList = new ArrayList<>();
        while (iterator.hasNext()) {
            rowsList.add(iterator.next());
        }
        final List<Tuple3<Double, Double, Double>> collectedList = rowsList.stream().map(row -> {
            Double lon = row.getDouble(row.fieldIndex("lon"));
            Double lat = row.getDouble(row.fieldIndex("lat"));
            Double speed = row.getDouble(row.fieldIndex("speed"));

            return new Tuple3<Double, Double, Double>(speed, lat, lon);
        }).collect(Collectors.toList());
       return collectedList;
    }

}
