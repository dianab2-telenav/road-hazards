package data_access;


import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;


public class JsonReader {

    private final static Logger LOGGER = Logger.getLogger(JsonReader.class.getName());

    public Dataset<Row> read(final SparkSession sparkSession, final String file) {
        LOGGER.log(Level.INFO,
                "[" + this.getClass().getSimpleName().toUpperCase() + "] Data started to read from " + file + " at "
                        + LocalDateTime.now());
        final Dataset<Row> data = sparkSession.sqlContext().read().json(file);
        LOGGER.log(Level.INFO,
                "[" + this.getClass().getSimpleName().toUpperCase() + "] Data finished to read from " + file + " at "
                        + LocalDateTime.now());
        return data;
    }

}


